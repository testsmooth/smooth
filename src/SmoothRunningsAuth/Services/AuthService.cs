using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Google.Apis.Auth;
using SmoothRunningsAuth.Interfaces;
using SmoothRunningsAuth.Repositories;
using SmoothRunningsClassLibrary.Models;

namespace SmoothRunningsAuth.Services
{
    public class AuthService : IAuthService
    {
        private readonly IUserRepository _userRepository;

        public AuthService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }


        public async Task<string> Authenticate(GoogleJsonWebSignature.Payload payload)
        {
            return await FindUser(payload);
        }

        private async Task<string> FindUser(GoogleJsonWebSignature.Payload payload)
        {
            var user = await _userRepository.GetUser(payload.Email);

            if (user == null )
            {
                throw new Exception("User not found");
            }
            return user;
        }
    }
}