using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SmoothRunningsAuth.Interfaces;
using SmoothRunningsAuth.Services;
using SmoothRunningsAuth.Repositories;
using Amazon.DynamoDBv2;

namespace SmoothRunningsAuth

{
  public class Startup
  {
    public const string AppS3BucketKey = "AppS3Bucket";

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public static IConfiguration Configuration { get; private set; }

    // This method gets called by the runtime. Use this method to add services to the container test data
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllers();
      // MODIFIED CODE
      // Add S3 to the ASP.NET Core dependency injection framework.
      services.AddAWSService<Amazon.S3.IAmazonS3>();
      services.AddAWSService<IAmazonDynamoDB>();
      services.AddScoped<IUserRepository, UserRepository>();
      services.AddScoped<IAuthService, AuthService>();

#if DEBUG
      services.AddCors(opts =>
      {
        opts.AddPolicy("AllowAll", builder =>
        {
          builder
              .AllowAnyOrigin()
              .AllowAnyMethod()
              .AllowAnyHeader();
        });
      });
#endif
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

#if DEBUG
      app.UseCors("AllowAll");
#endif

      app.UseHttpsRedirection();

      app.UseRouting();

      app.UseAuthorization();

      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
  }
}
