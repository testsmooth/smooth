using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmoothRunningsAuth.Repositories
{
    public interface IUserRepository
    {
        Task<List<string>> GetAllUsers();
        Task<string> GetUser(string email);
        Task AddUser(string email);
        Task DeleteUser(string email);
    }
}