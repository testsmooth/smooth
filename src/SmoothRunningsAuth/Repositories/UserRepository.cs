// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Threading.Tasks;
// using Google.Apis.Auth;
// using SmoothRunningsAuth.DAL;
// using SmoothRunningsClassLibrary.Models;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.Model;

namespace SmoothRunningsAuth.Repositories
{
    public class UserRepository : IUserRepository
    {
        private const string TableName = "Users";

        private readonly IAmazonDynamoDB _amazonDynamoDb;

        public UserRepository(IAmazonDynamoDB amazonDynamoDb)
        {
            _amazonDynamoDb = amazonDynamoDb;
        }

        // This can be used if you want to run a dynamoDb locally.
        private async Task Initialise()
        {
            var request = new ListTablesRequest
            {
                Limit = 10
            };

            var response = await _amazonDynamoDb.ListTablesAsync(request);

            var results = response.TableNames;

            if (!results.Contains(TableName))
            {
                var createRequest = new CreateTableRequest
                {
                    TableName = TableName,
                    AttributeDefinitions = new List<AttributeDefinition>
                    {
                        new AttributeDefinition
                        {
                            AttributeName = "Email",
                            AttributeType = "S"
                        }
                    },
                    KeySchema = new List<KeySchemaElement>
                    {
                        new KeySchemaElement
                        {
                            AttributeName = "Email",
                            KeyType = "HASH"  //Partition key
                        }
                    },
                    ProvisionedThroughput = new ProvisionedThroughput
                    {
                        ReadCapacityUnits = 2,
                        WriteCapacityUnits = 2
                    }
                };

                await _amazonDynamoDb.CreateTableAsync(createRequest);
            }
        }

        public async Task<List<string>> GetAllUsers()
        {
            var request = new ScanRequest
            {
                TableName = TableName,
            };

            var response = await _amazonDynamoDb.ScanAsync(request);

            return response.Items.Select(d => d["Email"].S).ToList();
        }

        public async Task<string> GetUser(string email)
        {
            var request = new GetItemRequest
            {
                TableName = TableName,
                Key = new Dictionary<string, AttributeValue> { { "Email", new AttributeValue { S = email } } }
            };

            var response = await _amazonDynamoDb.GetItemAsync(request);

            if (!response.IsItemSet)
                return null;

            return response.Item["Email"].S;
        }

        public async Task AddUser(string email)
        {
            var request = new PutItemRequest
            {
                TableName = TableName,
                Item = new Dictionary<string, AttributeValue>
                {
                    { "Email", new AttributeValue { S = email }}
                }
            };

            await _amazonDynamoDb.PutItemAsync(request);
        }

        public async Task DeleteUser(string email)
        {
            var request = new DeleteItemRequest
            {
                TableName = TableName,
                Key = new Dictionary<string, AttributeValue> { { "Email", new AttributeValue { S = email } } }
            };

            var response = await _amazonDynamoDb.DeleteItemAsync(request);
        }
    }
}