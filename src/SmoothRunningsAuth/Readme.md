# Smooth Runnings test data

SmoothRunnings is an internal application for the onboarding of new Andi's this application was created to automate the manual processes being performed by the people ops and payroll. You can find the [repositories here](https://gitlab.com/ANDigital/newton/smooth-runnings) and the [arcitecture diagram here](https://drive.google.com/file/d/1I1LP5yQ32QJZQSvvD-OT_GDOFHftbq7c/view?usp=sharing). 
 
So what does it actually do;  
- Firstly when a new Andi is set to hired in workable the application will pull the workable data and store them in the database.  
- We will then post their data to peopleHr( this will perform an update if they were already created and haven't submitted their payroll information.  
- While all of this is happening payroll and people ops will receive slack notifications informing them of new starters and any issues with their data.  
- 10 days before the Andi is due to start we will send out an email with a link to to the payroll form.
- This information will then be posted directly to PeopleHr we do not store any of this data (Payroll can view a list of people who haven't submitted their payroll form in the admin portal and can resend this at any point).
- On the day that the Andi starts the elapseit service will create their profile and delete all data we held about them
___
## Running the services
___
You will need the [Dotnet core 3.1 SDK](https://dotnet.microsoft.com/download/dotnet-core/3.1) to run the services.  

Once you have this you will have to also add the Newton Nuget source 

```bash
dotnet nuget add source 'http://newton-nuget-282390609.eu-west-2.elb.amazonaws.com/v3/index.json'
```
You will also need IAM credentials for the AND Newton AWS with SmoothRunnings access. Once you have these add them to your credentials files 
```bash
open ~/.aws/credentials
```
Also ensure that your Config file is set to the correct region
```bash
open ~/.aws/config
```
```javascript
[default]
region=eu-west-2
output=json
```
Any issues with this then just check out the [AWS Documentation](https://docs.aws.amazon.com/cli/index.html)
___
## Usage
___
If you need to debug more than one service locally then you can change the endpoints in startup.cs from the service you're posting from.  
**(Just Make sure you change them back)**  

Local Endpoints
- *Routing api* : https://localhost:5001
- *Slack service* : https://localhost:5003
- *Workable service* : https://localhost:5005
- *PeopleHr service* : https://localhost:5007
- *Email service* : https://localhost:5009
- *Auth service* : https://localhost:5011
- *Elapseit service* : Has no exposed endpoint will just run locally
___
## About the services
___
**Routing Api -**  *.net api*  
The routing api is the main entry point to the application from the client and handles all of the routing as well as being an additional security layer for the other services. This service also authenticates a new Andi when they try and fill in a payroll form. Currently a .net api (Looking to move to a lambda in future depending on start times).

**Slack Service -**  *.net api lambda*  
The slack service is connected to most of the services and will send slack messages to the relivant slack channels unless it detects you are running locally then will post all messages to a test channel (speak to one of the contacts below for access to this).

**Workable Service -** *.net api lambda*  
The workable service recieves a workable Id from the routing api and will then retireve the Andi's data and either add them or update them in the database. Once this has finished or if there is an error then it will send a slack message to the relevant channel. (This will always be the live workable data so please ask one of the contacts below for a test Id).

**PeopleHR Service -** *.net api lambda*  
The peoplehr service is responsible for 2 tasks firstly once data has been recieved from workable and stored in the database then the peoplehr service will create a peoplehr profile or update one if the Andi hasn't yet submitted their payroll from, this is to prevent overwriting data that is already there. Then once the payroll form is submitted the service will submit the payroll data without storing any of this information. Once these tasks complete this serivice will also post to the relevant slack channels.

**Email Service -** *.net api lambda*  
The email service has a scheduled task set to run daily, it will perform a check on if anyone is due to start in the next 10 days and if they are then an email will be produced and sent to the new Andi. Payroll also have the ability to resend these emails from the admin portal in the frontend.

**Auth Service -** *.net api lambda*  
The auth service serves 2 purposes, firstly to authenticate users from the frontend which it does using google auth and then checks the users email against a list of autherised emails stored in a dynamoDb. Once the user is authenticated they can also then go through the routing api to resend emails. They can also perform CRUD operations on the list of authenticated emails. (We have no access to the live system so it is okay to add yourself to this list).

**Elapseit Service -** *.net lambda*  
The elapseit service has no exposed endpoint this service will just run daily and find Andi's that are due to start that day. The service will then create Elapseit accounts for the new starters and delete any data we have stored for them. Once complete this service will send slack messages to the relevent channels.
___
## Contributing
___
Pull requests are welcome. For major changes, please open an issue first to discuss with the contacts below what you would like to change.

Please make sure to update tests as appropriate.
___
## Contacts
___
- Tom Adams - Newton - C# Developer
- Tom Kelly - Spark - Frontend Developer
- Yusuf Bismillah - Newton - Devops
- Pete Hartley - Newton - Analyst
