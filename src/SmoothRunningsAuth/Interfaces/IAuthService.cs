using System.Threading.Tasks;
using Google.Apis.Auth;
using SmoothRunningsClassLibrary.Models;

namespace SmoothRunningsAuth.Interfaces {
    public interface IAuthService
    {
        Task<string> Authenticate(GoogleJsonWebSignature.Payload payload);
    }
}