using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SmoothRunningsAuth.Interfaces;
using SmoothRunningsAuth.Models;
using SmoothRunningsClassLibrary.Auth;
using SmoothRunningsAuth.Helpers;

namespace SmoothRunningsAuth.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : ControllerBase
    {
        private IAuthService _authService;
        private AWSSecretService secretService = new AWSSecretService();
        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        [AllowAnonymous]
        [HttpPost("google")]
        public async Task<IActionResult> GoogleAsync([FromBody] UserView userView)
        {
            try
            {
                var payload = GoogleJsonWebSignature.ValidateAsync(userView.tokenId, new GoogleJsonWebSignature.ValidationSettings()).Result;
                var userEmail = await _authService.Authenticate(payload);

                var claims = new[] {
                    new Claim(JwtRegisteredClaimNames.Sub, Security.Encrypt((await secretService.GetSecrets()).JwtKey, userEmail)),
                    new Claim(JwtRegisteredClaimNames.Jti, SecurityAlgorithms.HmacSha256)
                };

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes((await secretService.GetSecrets()).JwtKey));
                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    string.Empty, string.Empty,
                    claims,
                    expires: DateTime.Now.AddSeconds(55 * 60 * 24),
                    signingCredentials: credentials
                );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });

            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }
            return BadRequest();
        }
    }
}