using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SmoothRunningsAuth.Repositories;
using SmoothRunningsClassLibrary.Auth;
namespace SmoothRunningsAuth.Controllers
{
  [ApiController]
  [Route("api/[controller]")]
  [BasicAuthentication]
  public class AdminController : ControllerBase
  {
    private readonly IUserRepository _userRepository;
    public AdminController(IUserRepository userRepository)
    {
      _userRepository = userRepository;
    }

    [HttpGet]
    [Route("GetAllUsers")]
    public async Task<List<string>> GetAllUsers()
    {
      return await _userRepository.GetAllUsers();
    }

    [HttpPost("AddUser/{email}")]
    public async Task AddUser(string email)
    {
      await _userRepository.AddUser(email);
    }

    [HttpDelete("DeleteUser/{email}")]
    public async Task DeleteUser([FromRoute] string email)
    {
      await _userRepository.DeleteUser(email);
    }
  }
}